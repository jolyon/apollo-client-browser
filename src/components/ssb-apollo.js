const tagName = 'ssb-apollo'
const template = document.createElement('template')
template.innerHTML = `<input id="input" placeholder="building ssb-apollo to make development of ssb apps joyful">
                      <img src="https://raw.githubusercontent.com/evbogue/mvd/master/mvd.png"/>`

class SsbApollo extends HTMLElement {
  constructor() {
    super()
  }

  connectedCallback() {
    // Check if shadowRoot exists first
    if (!this.shadowRoot) {
      this.attachShadow({mode: 'open'})
      this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    this.testSsbApolloWhoami()
  }

  testSsbApolloWhoami() {
    console.log('apollo-client', window.__APOLLO_CLIENT__)
  }
}
customElements.define(tagName, SsbApollo)
