# apollo client built for the browser

## goal is to build web components seamlessly integrated with apollo

### this repository builds a bundle to load apollo in the browser and also shows you how to include a simple vanilla js web component to get up and running quickly

<hr>

```bash
git clone
npm i
npm run build:apollo
```

public folder contains the bundle, source map and an index.html to see it working:

```bash
npm run serve
```

if it's not working for you it's probably because you're not using an evergreen firefox or chromium based browser.
see     <p>you may need web components polyfills for older browsers see: https://github.com/webcomponents/webcomponentsjs

<hr>

Thanks to [@bennypowers](https://github.com/bennypowers) for solving the bundling. After I got a working build in a very hacky non-optimal way with rollup I discovered his work using lit-html web components with apollo. Shamelessly borrowed some of your work! Thanks @bennypowers. This lead me to his excellent series of articles on building web components with various libraries but starting out with vanilla js. Highly recommend you check it out: https://dev.to/bennypowers/lets-build-web-components-part-1-the-standards-3e85
